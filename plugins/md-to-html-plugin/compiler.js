const { randomNum } = require('./utils');

const reg_mark = /^(.+?)\s/; // 以空字符串开头  以空格结尾
const reg_sharp = /^\#/; // 以#号开头
const reg_crossbar = /^\-/; // 以-开头
const reg_number = /^\d/; // 以数字开头的

// 创建树形结构
function createTree(mdArr) {
    let _htmlPool = {};
    let _lastMark = '';
    let _key = 0;

    mdArr.forEach((mdFragment) => {
        const matched = mdFragment.match(reg_mark);
         
        // 因为存在null，所以真才处理
        if(matched) {
            const mark = matched[1],
                  input = matched['input'];

            // 以#号开头
            if(reg_sharp.test(mark)) {
                // 有几个#号就是h几
                const tag = `h${mark.length}`;
                // 获取内容
                const tagContent = input.replace(reg_mark, '');
 
                if(_lastMark == mark) {
                    _htmlPool[`${tag}-${_key}`].tags = [..._htmlPool[`${tag}-${_key}`].tags, `<${tag}>${tagContent}</${tag}>`]
                } else {
                    _lastMark = mark;
                    _key = randomNum();
                    _htmlPool[`${tag}-${_key}`] = {
                        type: 'single',
                        tags: [`<${tag}>${tagContent}</${tag}>`]
                    }
                }
            }

            // 以-开头
            if(reg_crossbar.test(mark)) {
                const tagContent = input.replace(reg_mark, '');
                
                const tag = `li`;

                // 判断当前上一次的mark是否匹配正则
                if(reg_crossbar.test(_lastMark)) {
                    // 合并当前分类下的数据
                    _htmlPool[`ul-${_key}`].tags = [..._htmlPool[`ul-${_key}`].tags, `<${tag}>${tagContent}</${tag}>`]
                } else {
                    _lastMark = mark;
                    _key = randomNum();
                    _htmlPool[`ul-${_key}`] = {
                        type: 'wrap',
                        tags: [`<${tag}>${tagContent}</${tag}>`]
                    }
                }
            }

            // 以数字开头
            if(reg_number.test(mark)) {
                 
                // 替换掉前面的内容 以空字符串开头中间至少有一位，以空格结尾的匹配出来，替换成空 
                const tagContent = input.replace(reg_mark, '');
                const tag = `li`;
          
                // 判断
                if(reg_number.test(_lastMark)) {
                    _htmlPool[`ol-${_key}`].tags = [..._htmlPool[`ol-${_key}`].tags, `<${tag}>${tagContent}</${tag}>`]
                } else {
                    _lastMark = mark;
                    _key = randomNum();
                    _htmlPool[`ol-${_key}`] = {
                        type: 'wrap',
                        tags: [`<${tag}>${tagContent}</${tag}>`]
                    }
                }
            }
        }
    });

    return _htmlPool;
}


function compileHTML(_mdArr) {
    const _htmlPool = createTree(_mdArr);
    let _htmlStr = '';
    let item;
    // console.log(_htmlPool);
    for(var k in _htmlPool) {
        // console.log(k, _htmlPool[k]);
        item = _htmlPool[k];
        // console.log(item);
        if(item.type === 'single') {
            // 遍历拼接type 为single的html字符串

            item.tags.map(tag => {
                console.log(tag);
                _htmlStr += tag;
            })
        } 
        else if(item.type === 'wrap') {
            let _list = `<${k.split('-')[0]}>`;

            item.tags.forEach(tag => {
                _list += tag;
            });

            _list += `</${k.split('-')[0]}>`;

            _htmlStr += _list;
        }
    }

    // console.log(_htmlStr);
    return _htmlStr;
}

module.exports = {
    compileHTML
}

/**
 * {
 *   h1: {
 *      type: 'single',
 *      tags: [<h1>这是一个h1的标题</h1>]
 *   },
 *   ul: {
 *      type: 'wrap'
 *      tags: [
 *        '<li>这是UL列表的第1项</li>',
 *        '<li>这是UL列表的第1项</li>',
 *        '<li>这是UL列表的第1项</li>',
 *        '<li>这是UL列表的第1项</li>',
 *      ]
 *   }
 * }
 */