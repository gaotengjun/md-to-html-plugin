const { readFileSync } = require('fs');
const { resolve } = require('path');
const { compileHTML } = require('./compiler');

const INNER_MARK = '<!-- inner -->';

class MdToHtmlPlugin {
    constructor({ template, filename }) {

        // 用户没有传入模板 抛出错误
        if(!template) {
            throw new Error('The config for "template" must be configured');
        }

        this.template = template;
        this.filename = filename ? filename : 'md.html';
    }

    // webpack为每个插件提供一个方法apply
    // 编译的过程中都是在apply中做的
    // 有一个参数编译器 compiler

    apply(compiler) {
        // 编译器 下的hooks 下有个发射器
        compiler.hooks.emit.tap('md-to-html-plugin', (compilation) => {
            const _assets = compilation.assets;
            console.log(_assets);
            // 读取用户webpack中配置的模板文件
            const _mdContent = readFileSync(this.template, 'utf8');
            // 读取模板html
            const _templateHTML = readFileSync(resolve(__dirname, 'template.html'), 'utf8')
            // 将md文件一行一行的存储到数组中
            const _mdContentArr = _mdContent.split('\n');
       
            // 编译md字符串为html字符串
            const _htmlStr = compileHTML(_mdContentArr);

            console.log(_htmlStr);
            // 将注释替换成md字符串转换成的html字符串
            const _finalHTML = _templateHTML.replace(INNER_MARK, _htmlStr); 

            // 在_assets上增加资源
            _assets[this.filename] = {
                source() { // 方法返回一个资源
                    return _finalHTML; 
                },
                size() { // 一般return 资源的长度
                    return _finalHTML.length;
                }
            }

            // console.log(_assets);
        })
    }
}


module.exports = MdToHtmlPlugin;